#ifndef NEWWORK_COMM_H
#define NEWWORK_COMM_H

;
#pragma pack(push) //保存对齐状态
#pragma pack(1)
//客户端与服务器通信结构体
#define MSG_SIZE  1000

enum CMDTYPE{
    //-----登录相关
     USER_LOGIN,
     USER_DEL,
     USER_EDIT,
    //------------聊天相关
     TALK_SIGNAL,
     TALK_ALL,
    //------服务器返回相关
     RET_STATUS
 };


//struct Msg_head  //消息头结构
//{
//    short int head;
//    unsigned int msgSize;
//    unsigned char msgType;
//};


//注册或修改
struct Login_regStruct
{
    struct Msg_head;
    unsigned char  sid[10];
    unsigned char  name[30];
    unsigned char  pwd[20];
    unsigned char  img[MSG_SIZE];
    unsigned char  sf_type;
};

//登录，删除
struct Login_logStruct
{
    struct Msg_head;
     char  sid[10];
    char  pwd[20];
};



struct Msg_talk
{
    struct Msg_head;
    unsigned char  rid[10]; //接收人的sid
    unsigned char  content[MSG_SIZE];
    unsigned char  sender[30]; //发送人姓名
    unsigned char  s_time[30]; //消息发送时间
};




union Msg_ret
{
    struct Msg_head;
    bool  ret_status;             //响应状态码
    unsigned char record[30];     //返回信息说明
};


#pragma pack(pop) // 恢复先前的pack设置
#endif // NEWWORK_COMM_H
