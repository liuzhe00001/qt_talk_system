#ifndef DATEBAASE_MANAGE_H
#define DATEBAASE_MANAGE_H
#include<QtSql>
#include<QSqlDatabase>

class Datebaase_Manage
{
   public:
         QSqlDatabase conn;
         QSqlQuery query;

         Datebaase_Manage();
        ~Datebaase_Manage();
         void insert_data(QString insert_str);
         void update_data(QString  update_str);
         void delete_data(QString  delete_str);
         QSqlQuery select_data(QString  select_str);

};

#endif // DATEBAASE_MANAGE_H
