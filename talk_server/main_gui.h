#ifndef MAIN_GUI_H
#define MAIN_GUI_H

#include <QMainWindow>
#include<QJsonObject>
#include<QJsonDocument>

#include<QTcpServer>
#include<QTcpSocket>
#include<QHostAddress>

#include "global.h"
#include "database_mange.h"

QT_BEGIN_NAMESPACE
namespace Ui { class main_gui; }
QT_END_NAMESPACE

class main_gui : public QMainWindow
{
    Q_OBJECT

public:
    QTcpServer *ser; //服务端监听套接字
    QTcpSocket *cli; //客户端套接字
    QMap<QString,QTcpSocket> conn_set;//保存在线链接
    Database_Mange *db_obj;  //创建连接数据库

    main_gui(QWidget *parent = nullptr);
    ~main_gui();  

    void response_login(QJsonObject &data);  //登录
    void response_reg(QJsonObject &data);    //注册
    void response_SingleTalk();    //聊天


public slots:
    void rev_conn();   //接收连接
    void rev_data() ;  //接收数据
    void response_client(QJsonObject rdata);  //回复客户端

signals:
     void handle_recv(QJsonObject rdata);

private slots:
     void on_pushButton_clicked();

private:
    void show_msg(QByteArray data);


private:
    Ui::main_gui *ui;
};



#endif // MAIN_GUI_H
