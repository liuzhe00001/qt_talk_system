#include "ser_comm.h"

Ser_comm::Ser_comm(QHostAddress addr,quint16 port)
{
    Q_UNUSED(addr);
    Q_UNUSED(port);

    ser=new QTcpServer;

    ser->listen(QHostAddress("127.0.0.1"),8000);

    connect(ser,&QTcpServer::newConnection,this,&Ser_comm::rev_conn);

}
#include<QApplication>
//接收连接
 void Ser_comm:: rev_conn()
 {

     cli=ser->nextPendingConnection();
     connect(cli,&QTcpSocket::readyRead,this,&Ser_comm::rev_data);

 }

 //接收数据
  void Ser_comm::rev_data()
  {
     QByteArray rdata=((QTcpSocket*)sender())->readAll();
    qDebug()<<"读书签将"<<endl;
    
      emit data_sig(rdata);//读到数据后，发送信号给主窗体，便于主窗体进行操作
  }

  //发送数据
  void Ser_comm::send_data(const char* data,quint64 size)
  {
      cli->write(data,size);

  }

  Ser_comm::~ Ser_comm()
  {

      delete ser;

  }
