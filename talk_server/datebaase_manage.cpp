#include "datebaase_manage.h"

Datebaase_Manage::Datebaase_Manage():query(conn)
{

    qDebug()<<QSqlDatabase::drivers()<<endl;
    conn=QSqlDatabase::addDatabase("QODBC");//创建数据库连接，并为其命名testConnect

   ////连接数据库主机名，这里需要注意（若填的为”127.0.0.1“，出现不能连接，则改为localhost)
    conn.setHostName("localhost");      //连接数据库主机名，这里需要注意（若填的为”127.0.0.1“，出现不能连接，则改为localhost)
    conn.setDatabaseName("test_odbc"); //指定的是ODBC的数据源名，而不是数据库名
    conn.setPort(3306);                 //连接数据库端口号，与设置一致
    conn.setUserName("root");          //数据库用户名，与设置一致
    conn.setPassword("198552");    //数据库密码，与设置一致
    bool ok=conn.open();
   if (ok) {
      qDebug()<<"连接数据库成功"<<endl;}
   else
      {qDebug()<<"连接数据库失败"<<endl;abort();}


   //      QSqlQuery query( database);
   //      query.exec("select * from stu");
   //     QSqlRecord rec;
   //     while(query.next())
   //     {
   //         rec = query.record();
   //         //QVariant转其他类型
   //         QString value2 = query.value(1).toString();

   //         qDebug()<<value2<<endl;
   //     }

}

QSqlQuery Datebaase_Manage::select_data(QString  select_str)
{
    query.exec(select_str);

    return query;

}

void Datebaase_Manage::insert_data(QString insert_str)
{
   query.exec(insert_str);
}

 void Datebaase_Manage::update_data(QString  update_str)
 {
     query.exec(update_str);
 }


 void Datebaase_Manage::delete_data(QString  delete_str)
 {
     query.exec(delete_str);
 }

 Datebaase_Manage::~Datebaase_Manage()
 {
     conn.close();

 }


