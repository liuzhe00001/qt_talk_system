#ifndef DATEBAASE_MANAGE_H
#define DATEBAASE_MANAGE_H
#include<QtSql>
#include<QSqlDatabase>
#include<QSqlQuery>

class Database_Mange
{
   public:
         QSqlDatabase conn;  //创建数据库连接
         QSqlQuery *query;   //数据库操作对象

         Database_Mange();
        ~Database_Mange();
         bool insert_data(QString insert_str);
         void update_data(QString  update_str);
         void delete_data(QString  delete_str);

         //QSqlQuery& select_data(QString  select_str);
         QSqlQuery* select_data(QString  select_str);

};

#endif // DATEBAASE_MANAGE_H
