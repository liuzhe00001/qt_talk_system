#include "main_gui.h"
#include "ui_main_gui.h"
#include<QDir>

main_gui::main_gui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::main_gui)
{
    ui->setupUi(this);


    //创建tcp服务端
    ser=new QTcpServer;
    ser->listen(QHostAddress("127.0.0.1"),8000);
    //接收网络连接
    connect(ser,&QTcpServer::newConnection,this,&main_gui::rev_conn);

    //响应接收的数据
    connect(this,&main_gui::handle_recv,this,&main_gui::response_client);

    db_obj=new Database_Mange;

}
//接收连接
 void main_gui:: rev_conn()
 {

     cli=ser->nextPendingConnection();
     connect(cli,&QTcpSocket::readyRead,this,&main_gui::rev_data);

 }

//接收数据
 void main_gui::rev_data()
 {
     qDebug()<<"ser:"<<"1111"<<endl;
    QByteArray rdata=((QTcpSocket*)sender())->readAll();
     qDebug()<<rdata<<endl;
    QJsonDocument doc=QJsonDocument::fromJson(rdata);
    QJsonObject obj=doc.object();

    emit handle_recv(obj);



 }



//注册
void main_gui::response_reg(QJsonObject &rdata)
{
    qDebug()<<"****************************88"<<endl;

    QJsonObject obj;
    bool sucess_flag=false;

   //缓存图片
   QDir dir;
   QString cur_path=QDir::currentPath();
   QString img_path=cur_path+"/img_file";//文件名和路径做一个拼接
   if(dir.exists(img_path)==false)
       dir.mkdir(img_path);

   //解析数据
   bool find_flag=false;
   QString uid=rdata["uid"].toString();

  //扫描数据库不存在则添加数据
  QSqlQuery* query= db_obj->select_data(QString("select uid from user_table"));
  QSqlRecord rec;
   while(query->next())
       {
           rec = query->record();
           qDebug()<<rec.value(0).toString()<<endl;
           if(rec.value(0).toString()==uid)
              {
               qDebug()<<"find sucesss"<<endl;
               find_flag=true;
               break;
               }
       }


   if(find_flag==false)
   {
       qDebug()<<"插入数据:"<<img_path<<endl;
       //服务器保存客户端发过来的图片
       img_path += QString("/%1.png").arg(uid);
       //img_path=img_path+QString("/%1.png").arg(uid);
       QFile file(img_path);
       bool tt=file.open(QIODevice::WriteOnly);

       QDataStream out(&file);
       QByteArray img_data=rdata["img"].toVariant().toByteArray();
        qDebug()<<"tt"<<tt<<img_data.size()<<endl;
       out.writeRawData(QByteArray::fromBase64(img_data),img_data.size());
       file.close();


      //向表中插入数据
     QString str=QString("insert into user_table(uid,uname,pwd,utype,img_path,user_status,online_status)values('%1','%2','%3',%4,'%5',%6,%7)")
             .arg(rdata["uid"].toString()) //QString中的arg方法类似于“C中的printf中使用的格式输出符”和“C++中string的append方法”的结合体。
             .arg(rdata["name"].toString())
             .arg(rdata["pwd"].toString())
             .arg(rdata["utype"].toInt())
             .arg(img_path)
             .arg(0)
             .arg(0);
     qDebug()<<str<<endl;
     bool cc=query->exec(str);

     qDebug()<<"cc"<<cc<<endl;

      sucess_flag=true;

   }


   //回复客户端端
   obj["msgType"]=TALK::USER_REG_RET;
   obj["rtval"]=sucess_flag;

   QJsonDocument wdata(obj);
   QByteArray data=wdata.toJson();
   cli->write(data,data.size());
   //缓存区
   cli->flush();
  // qDebug()<<data<<endl;

}
//登录
void main_gui::response_login(QJsonObject &rdata)
{

       qDebug()<<"lgoin;"<<endl;
         bool sucess_flag=false;   //登录成功标志
         bool find_flag=false;     //寻找成功标志
         QJsonObject obj;          //JSON对象

         QString uid=rdata["uid"].toString();
         QString pwd=rdata["pwd"].toString();

        QSqlQuery* query= db_obj->select_data(QString("select * from user_table"));
        QSqlRecord rec;
         while(query->next())
             {
                 rec = query->record();
                 qDebug()<<rec.value(0).toString()<<":"<<rec.value(2).toString()<<endl;
                 if((rec.value(0).toString()==uid) &&(rec.value(2).toString()==pwd))
                    {
                     qDebug()<<"login check sucesss"<<endl;
                     find_flag=true;
                     break;
                     }
             }

     if(find_flag==true)
     {
         sucess_flag=true;
     }
     else
         sucess_flag=false;

     //回复客户端端
     obj["msgType"]=TALK::USER_LOGIN_RET;
     obj["rtval"]=sucess_flag;

     QJsonDocument wdata(obj);
     QByteArray data=wdata.toJson();
     cli->write(data,data.size());
     qDebug()<<"ser"<<obj<<endl;
     cli->flush();
}

 void main_gui::show_msg(QByteArray data)
 {





 }


 void main_gui::response_client(QJsonObject rdata)
 {
     qDebug()<<"2222"<<rdata<<endl;
     switch (rdata["msgType"].toInt())
     {
       case TALK::USER_REG: response_reg(rdata);break;
       case TALK::USER_LOGIN:response_login(rdata) ;break;
       case TALK::USER_EDIT: ;
       case TALK::USER_DEL: ;
       case TALK::USER_TALK_SINGLE: ;
       case TALK::USER_TALK_ALL: ;
     }

 }


main_gui::~main_gui()
{
    delete ui;
    //delete conn;
}


void main_gui::on_pushButton_clicked()
{
    QJsonObject obj;
    obj["msgType"]=TALK::USER_REG_RET;
    obj["rtval"]=true;

    QJsonDocument wdata(obj);
    QByteArray data=wdata.toJson();
    cli->write(data,data.size());
    //cli->flush();
}
