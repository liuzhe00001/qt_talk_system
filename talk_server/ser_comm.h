#ifndef SER_COMM_H
#define SER_COMM_H
#include<QObject>

//服务器端通信基础类
class Ser_comm:public QObject
{
    Q_OBJECT

public:
    QTcpServer *ser; //服务端监听套接字
    QTcpSocket *cli; //客户端套接字
    Ser_comm(QHostAddress addr,quint16 port);
    ~Ser_comm();
    void send_data(const char* data,quint64 size);

public slots:
    void rev_conn();
    void rev_data() ;

signals:
    void data_sig(QByteArray rdata);
};

#endif // SER_COMM_H
