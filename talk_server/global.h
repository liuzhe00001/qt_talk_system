#ifndef GLOBAL_H
#define GLOBAL_H
namespace TALK
{

enum mType {
    USER_REG=1,  //用户注册
    USER_LOGIN=2, //用户登录
    USER_EDIT=3,  //用户退出
    USER_DEL=4,  //用户删除

    USER_REG_RET=10,
    USER_LOGIN_RET=10,
    USER_EDIT_RET=10,
    USER_DEL_RET=10,
   //----------添加好友
    USER_ADD=7,    //用户添加
    USER_TALK_SINGLE=8,  //用户聊天信号
    USER_TALK_ALL=9  //群聊

 };



}
#endif // GLOBAL_H
