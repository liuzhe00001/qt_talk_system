#ifndef MAIN_GUI_H
#define MAIN_GUI_H

#include <QMainWindow>
#include <QIcon>
#include <QToolButton>
#include <QMessageBox>
#include "chat_widget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class main_gui; }
QT_END_NAMESPACE

class main_gui : public QMainWindow
{
    Q_OBJECT

public:
    main_gui(QWidget *parent = nullptr);
    ~main_gui();

private slots:


private:
    Ui::main_gui *ui;
    //用容器保存这些人是否被弹出
    QVector<bool>isShow;
};
#endif // MAIN_GUI_H
