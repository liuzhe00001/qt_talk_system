#include "user_register.h"
#include "ui_user_register.h"

user_register::user_register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::user_register)
{
    ui->setupUi(this);

    img_path="";
   //建立连接
    //cli=new QTcpSocket(this);
    //cli->connectToHost(QHostAddress("127.0.0.1"),8000);

    connect(&cli,&QTcpSocket::readyRead,this,&user_register::rev_data);
}

user_register::~user_register()
{
    delete ui;
    //delete cli;
}

 void user_register::rev_data()
 {
     qDebug()<<"c_file"<<endl;

     QByteArray msgdata= ((QTcpSocket*)sender())->readAll();
     QJsonDocument doc=QJsonDocument::fromJson(msgdata);
     QJsonObject obj= doc.object();


     if(obj["rtval"].toBool())
         QMessageBox::about(this,"警告","注册成功");
     else
         QMessageBox::about(this,"警告","注册失败");


 }

//上传图片并显示
void user_register::on_btn_upload_clicked()
{
    img_path= QFileDialog::getOpenFileName();
    QPixmap img;
    img.load(img_path);
    ui->lbl_img->setPixmap(img);
}

//传递注册信息
void user_register::on_btn_ok_clicked()
{
    //从界面获取数据
    QString name=ui->txt_name->text().trimmed();
    QString uid=ui->txt_uid->text().trimmed();
    QString pwd=ui->txt_pwd->text().trimmed();

    quint8 user_type=0;//账户类型
    if(ui->rd_user->isChecked())
       user_type=0;
    else
       user_type=1;


    //json传输图片，转base64格式
    QFile file(img_path);
    file.open(QIODevice::ReadOnly);
    QByteArray img_data=file.readAll().toBase64();
    file.close();

    //封装数据
    QJsonObject obj;
    obj["msgType"]=TALK::USER_REG;
    obj["name"]=name;
    obj["pwd"]=pwd;
    obj["uid"]=uid;
    obj["utype"]=user_type;
    obj["img"]=QJsonValue::fromVariant(img_data);//转出成QJSONvalue值

    //发送数据
    QJsonDocument wdata(obj);
    QByteArray data=wdata.toJson();
    qDebug()<<"test"<<data<<endl;
    cli.write(data,data.size());

}



void user_register::on_btn_exit_clicked()
{
    disconnect(&cli,&QTcpSocket::readyRead,this,&user_register::rev_data);//解决防冲突，保证任何时候只有一个类接收服务器数据
    emit close_regUi();

    this->close();
}
