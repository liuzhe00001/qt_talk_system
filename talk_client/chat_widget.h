#ifndef CHAT_WIDGET_H
#define CHAT_WIDGET_H

#include <QWidget>
#include <QUdpSocket>
#include <QMessageBox>
#include <QDateTime>
#include <QColorDialog>
#include <QFileDialog>

namespace Ui {
class chat_widget;
}

class chat_widget : public QWidget
{
    Q_OBJECT
    enum MsgType
    {
        Msg,   //发送消息类型
        UsrEnter,  //新用户进入类型
        UsrLeft  //用户离开消息
    };

public:
    explicit chat_widget(QWidget *parent,QString name);
    ~chat_widget();
    //关闭事件
    void closeEvent(QCloseEvent *event);

    void sndMsg(MsgType type);    //广播UDP消息
    void usrEnter(QString username);  //处理新用户加入
    void usrLeft(QString username,QString time); //处理用户离开
    QString getUsr();  //获取用户名
    QString getMsg();  //获取聊天信息

private:
    Ui::chat_widget *ui;

    QUdpSocket * udpSocket; //udp套接字

    qint16 port; //端口号

    QString uName;//用户名

    void ReceiveMessage(); //接收UDP消息

signals:
    //关闭窗口发送关闭信息
    void closechat();


};

#endif // CHAT_WIDGET_H
