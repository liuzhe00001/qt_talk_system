#include "login.h"
#include "ui_login.h"
#include<QHostAddress>

//客户端创建一个全局的客户端连接
QTcpSocket  cli;


login::login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::login)
{


    ui->setupUi(this);
    //cli=new QTcpSocket(this);
    cli.connectToHost(QHostAddress("127.0.0.1"),8000);
    connect(&cli,&QTcpSocket::readyRead,this,&login::read_data);

}


login::~login()
{
    delete ui;
    //delete cli;
}


void login::read_data()
{
   //解析服务器的响应数据
   QByteArray msgdata= ((QTcpSocket*)sender())->readAll();
   QJsonDocument doc=QJsonDocument::fromJson(msgdata);
   QJsonObject obj= doc.object();
   qDebug()<<"client="<<msgdata<<endl;
   if(obj["rtval"].toBool())//&& obj["msgType"].toInt()==TALK::USER_LOGIN_RET
     {
       QMessageBox::about(this,"警告","登录成功");
       emit sig_mainGuiSkip();//发送跳转界面信号
       this->close();//关闭窗口
     }
   else
       QMessageBox::about(this,"警告","登录失败");

}


void login::on_btn_login_clicked()
{
    //从登录框中获取数据
    QString sid=ui->txt_account->text().trimmed();
    QString pwd=ui->txt_pwd->text().trimmed();

    //封装数据
    QJsonObject obj;//Qmap
    obj["msgType"]=TALK::USER_LOGIN;
    obj["uid"]=sid;
    obj["pwd"]=pwd;

    //发送数据
    QJsonDocument wdata(obj);
    QByteArray data=wdata.toJson();
    qDebug()<<"login"<<data<<endl;
    cli.write(data,data.size());
}


// 显示注册界面
void login::on_btn_reg_clicked()
{
    emit sig_regGuiSkip();
   // ui_reg.show();
    disconnect(&cli,&QTcpSocket::readyRead,this,&login::read_data);

    //给注册界面绑定一个槽函数
   // connect(&ui_reg,&user_register::close_regUi,this, &login::start_conn);
    this->close();
}

//客户端连接服务器
 void login::start_conn()
 {
     this->show();
     connect(&cli,&QTcpSocket::readyRead,this,&login::read_data);
 }


void login::on_btn_exit_clicked()
{
    this->close();
}
