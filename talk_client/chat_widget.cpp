#include "chat_widget.h"
#include "ui_chat_widget.h"

chat_widget::chat_widget(QWidget *parent,QString name) :
    QWidget(parent),
    ui(new Ui::chat_widget)
{
    ui->setupUi(this);

    //初始化
    udpSocket = new QUdpSocket(this);

    //获取用户名
    uName = name;

    //获取端口号
    port = 9999;

    //绑定端口号  绑定模式  共享地址  掉线重连
    udpSocket->bind(port,QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);

    //发送新用户进入
    sndMsg(UsrEnter);

    //点击发送按钮发送消息
    connect(ui->sendBtn,&QPushButton::clicked,[=]()
    {
       sndMsg(Msg);
    });

    //监听别人发送的消息
    connect(udpSocket,&QUdpSocket::readyRead,this,&chat_widget::ReceiveMessage);

    //点击退出按钮，关闭窗口
    connect(ui->exitBtn,&QPushButton::clicked,[=]()
    {
        this->close();
    });

    ///////////////////////////////////////辅助功能//////////////////////////////////
    //字体设置
    connect(ui->fontCbx,&QFontComboBox::currentFontChanged,[=](const QFont &font)
    {
        ui->msgTxtEdit->setCurrentFont(font);
        ui->msgTxtEdit->setFocus();
    });

    //字号设置
    void (QComboBox:: * cbxsingal) (const QString &text) = &QComboBox::currentIndexChanged;
    connect(ui->sizeCbx,cbxsingal,[=](const QString &text)
    {
        ui->msgTxtEdit->setFontPointSize(text.toDouble());
        ui->msgTxtEdit->setFocus();
    });

    //加粗
    connect(ui->boldTBtn,&QPushButton::clicked,[=](bool isCheck)
    {
        if(isCheck)
        {
            ui->msgTxtEdit->setFontWeight(QFont::Bold);
        }
        else
        {
            ui->msgTxtEdit->setFontWeight(QFont::Normal);
        }
    });

    //倾斜
    connect(ui->italicTBtn,&QPushButton::clicked,[=](bool check)
    {
        ui->msgTxtEdit->setFontItalic(check);
    });

    //下划线
    connect(ui->underlineTBtn,&QPushButton::clicked,[=](bool check)
    {
        ui->msgTxtEdit->setFontUnderline(check);
    });

    //字体颜色
    connect(ui->colorTBtn,&QPushButton::clicked,[=]()
    {
        QColor color = QColorDialog::getColor(Qt::blue);
        ui->msgTxtEdit->setTextColor(color);
    });

    //保存聊天记录
    connect(ui->saveTBtn,&QPushButton::clicked,[=]()
    {
        if(ui->msgBrowser->document()->isEmpty())
        {
            QMessageBox::warning(this,"警告","内容不能为空！");
            return;
        }
        else
        {
            QString path = QFileDialog::getSaveFileName(this,"保存记录","聊天记录","(*.txt)");
            QFile file(path);
            //打开模式
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream stream(&file);
            stream << ui->msgBrowser->toPlainText();
            file.close();
        }
    });

    //清空聊天记录
    connect(ui->clearTBtn,&QPushButton::clicked,[=]()
    {
        ui->msgBrowser->clear();
    });
}

//广播UDP消息
void chat_widget::sndMsg(MsgType type)
{
    //发送消息的类型分为三种
    //发送的数据做分段处理
    //第一段 类型
    //第二段 用户名
    //第三段  具体内容
    QByteArray array;

    //创建流 可以分段 参数1流入的地址 参数2只能写
    QDataStream stream(&array,QIODevice::WriteOnly);

    stream<<type<<this->getUsr();  //类型放入到流中  用户名 和也放入流中

    switch (type) {
    case Msg:   //发送的普通消息
        if(ui->msgTxtEdit->toPlainText()=="") //判断如果用户没有输入，就不发消息
        {
            QMessageBox::warning(this,"警告","发送内容不能为空！");
            return ;
        }
        //第三段 具体内容
        stream<<this->getMsg();
        break;
    case UsrEnter: //新用户进入消息

        break;
    case UsrLeft:  //用户离开消息

        break;
    default:
        break;
    }

    //书写报文
    udpSocket->writeDatagram(array,QHostAddress::Broadcast,port);
}

//接收UDP消息
void chat_widget::ReceiveMessage()
{
    //获取长度
    qint16 size = udpSocket->pendingDatagramSize();
    //int mysize=static_cast<int>(size);
    QByteArray array = QByteArray(size,0);
    //接收数据报
    udpSocket->readDatagram(array.data(),size);

    //解析数据
    //第一段  类型  第二段 用户名  第三段  具体消息
    QDataStream stream(&array,QIODevice::ReadOnly);

    //获取当前时间
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    int msgType;  //读取到的类型
    QString userName;  //读取到的用户名
    QString msg;  //读取到的消息

    stream >>msgType;

    switch (msgType)
    {
        case Msg:   //接收的普通消息
            stream >> userName >>msg;

            //追加聊天记录
            ui->msgBrowser->setTextColor(Qt::red);  //显示的字体颜色
            ui->msgBrowser->append("["  +userName+  "]" + time); //显示用户名和时间
            ui->msgBrowser->append(msg);  //显示消息
            break;
        case UsrEnter:

            stream >> userName;
            usrEnter(userName);

            break;
        case UsrLeft:

            stream >> userName;
            usrLeft(userName,time);

            break;
        default:
            break;
    }


}
//处理新用户加入
void chat_widget::usrEnter(QString username)
{
    bool isempty = ui->usrTblWidget->findItems(username,Qt::MatchExactly).isEmpty();
    if(isempty)
    {
        QTableWidgetItem * usr = new QTableWidgetItem(username);

        //插入行
        ui->usrTblWidget->insertRow(0);
        ui->usrTblWidget->setItem(0,0,usr);
        //追加聊天记录
        ui->msgBrowser->setTextColor(Qt::gray);
        ui->msgBrowser->append(QString("%1 上线了").arg(username));
        //在线人数更新
        ui->usrNumLbl->setText(QString("在线人数：%1人").arg(ui->usrTblWidget->rowCount()));

        //把自身信息发送出去
        sndMsg(UsrEnter);
    }
}

//处理用户离开
void chat_widget::usrLeft(QString username,QString time)
{
    //更新右侧TableWidget
    bool  isempty = ui->usrTblWidget->findItems(username,Qt::MatchExactly).isEmpty();
    if(!isempty)
    {
        int row = ui->usrTblWidget->findItems(username,Qt::MatchExactly).first()->row();
        ui->usrTblWidget->removeRow(row);

        //追加聊天记录
        ui->msgBrowser->setTextColor(Qt::gray);
        ui->msgBrowser->append(QString("%1 于 %2 下线了").arg(username).arg(time));
        //在线人数更新
        ui->usrNumLbl->setText(QString("在线人数：%1人").arg(ui->usrTblWidget->rowCount()));


    }
}

//获取用户名
QString chat_widget::getUsr()
{
    return uName;
}

//获取聊天信息
QString chat_widget::getMsg()
{
    QString str = ui->msgTxtEdit->toHtml(); //这里用这个是因为 我使用了字体的改变

    //清空输入框
    ui->msgTxtEdit->clear();
    ui->msgTxtEdit->setFocus();


    return str;
}

//判断聊天窗口关闭事件
void chat_widget::closeEvent(QCloseEvent * event)
{
    emit this->closechat();

    sndMsg(UsrLeft);
    //关闭套接字
    udpSocket->close();
    udpSocket->destroyed();

}


chat_widget::~chat_widget()
{
    delete ui;
}


