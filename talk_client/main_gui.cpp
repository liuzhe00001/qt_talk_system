#include "main_gui.h"
#include "ui_main_gui.h"

main_gui::main_gui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::main_gui)
{
    ui->setupUi(this);

    //设置标题
    setWindowTitle("MYQQ");
    //设置图标
    setWindowIcon(QPixmap(":/new/prefix1/E:/images/QQ1 .png"));

    //准备图标
    QList<QString>nameList;
    nameList <<"小张"<<"小李";

    //图片资源列表
    QStringList iconNameList;
    iconNameList <<"男头像"<<"女孩";

    //容器保存
    QVector<QToolButton *>vToolBtn;

    for(int i = 0;i<2;i++)
    {
        //设置头像
        QToolButton * btn = new QToolButton;
        //设置文字
        btn->setText(nameList[i]);
        //设置头像
        QString str = QString(":/new/prefix/E:/images/%1.png").arg(iconNameList.at(i));
        btn->setIcon(QPixmap(str));
        //设置头像大小
        btn->setIconSize(QPixmap(str).size());
        //设置按钮风格透明
        btn->setAutoRaise(true);
        //设置文字和图片一起出现
        btn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        //添加到垂直布局
        ui->verticalLayout->addWidget(btn);

        //容器保存按钮，方便以后再操作
        vToolBtn.push_back(btn);

        //默认都没有被打开
        isShow.push_back(false);
    }

    for(int i = 0;i<vToolBtn.size();i++)
    {
        connect(vToolBtn[i],&QToolButton::clicked,[=]()//mutable
        {
            //如果被打开就不要再被打开了
            if(isShow[i])
            {
                QString str = QString("%1的窗口已 经被打开了").arg(vToolBtn[i]->text());
                QMessageBox::warning(this,"警告",str);
                return;
            }

            isShow[i] = true;
            //弹出聊天对话框
            //构造聊天窗口时候  要告诉这个窗口弹出的名字 参数1  顶层方式弹出  参数2  窗口名字
            //然而  chat_widget构造函数并没有这两个参数  就需要自己加
            chat_widget * chat = new chat_widget(0,vToolBtn[i]->text());
            //设置窗口标题
            chat->setWindowTitle(vToolBtn[i]->text());
            chat->setWindowIcon(vToolBtn[i]->icon());
            chat->show();

            //监听关闭信号
            connect(chat,&chat_widget::closechat,[=]()
            {
                isShow[i] = false;
            });

        });
    }

}

main_gui::~main_gui()
{
    delete ui;
}


