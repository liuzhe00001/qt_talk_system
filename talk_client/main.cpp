#include "main_gui.h"
#include "login.h"
#include "user_register.h"

#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    login lg;

    lg.show();

    user_register reg_gui;
    main_gui w;
    QObject::connect(&lg,&login::sig_mainGuiSkip,&w,&main_gui::show);//登录到主界面的跳转
    QObject::connect(&lg,&login::sig_regGuiSkip,&reg_gui,&user_register::show);//登录到注册面的跳转

    //解决服务器响应时，客户端出现两个对readyread信号的影响，造成冲突，关闭注册页面时断开注册页面类的读信号连接
    //或者在创建注册页面对象时，传递登录页面对象，检测页面是否看见来确定是否构建连接。
    QObject::connect(&reg_gui,&user_register::close_regUi,&lg, &login::start_conn);

    //w.show();

    return a.exec();
}
