#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>

#include<QTcpSocket>

#include<QHostAddress>

#include<QJsonObject>
#include<QJsonDocument>

#include "user_register.h"
#include "global.h"

namespace Ui {
class login;
}

class login : public QWidget
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = nullptr);
    ~login();
    //QTcpSocket *cli;

public slots:

    void read_data();
    void on_btn_login_clicked();
    void on_btn_reg_clicked();


    void start_conn();

    void on_btn_exit_clicked();

signals:
   void sig_mainGuiSkip();
   void sig_regGuiSkip();

private:
    Ui::login *ui;
};

//struct Msg_head  //消息头结构
//{
//    unsigned short head;//头字段// aabb
//    unsigned int msgSize;
//    unsigned char msgType;
//};

////登录，删除
//struct Login_logStruct
//{
//    Msg_head head;
//    char  sid[10];
//    char  pwd[20];
//};

#endif // LOGIN_H
