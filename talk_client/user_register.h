#ifndef USER_REGISTER_H
#define USER_REGISTER_H

#include <QWidget>
#include<QFileDialog>

#include<QJsonObject>
#include<QJsonDocument>

#include<QTcpSocket>
#include<QHostAddress>
#include <QFile>
#include<QMessageBox>
#include "global.h"

namespace Ui {
class user_register;
}

class user_register : public QWidget
{
    Q_OBJECT

public:
    explicit user_register(QWidget *parent = nullptr);
    ~user_register();

      //QTcpSocket *cli;
      QString img_path;

private slots:
    void rev_data();
    void on_btn_upload_clicked();
    void on_btn_ok_clicked();

    void on_btn_exit_clicked();

signals:
    void close_regUi();
private:
    Ui::user_register *ui;
};

#endif // USER_REGISTER_H
