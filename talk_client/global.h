#ifndef GLOBAL_H
#define GLOBAL_H
#include <QTcpSocket>
namespace TALK
{

enum mType {
    USER_REG=1,
    USER_LOGIN=2,
    USER_EDIT=3,
    USER_DEL=4,

    USER_REG_RET=10,
    USER_LOGIN_RET=11,
    USER_EDIT_RET=12,
    USER_DEL_RET=13,
   //----------添加好友
    USER_ADD=20,
    USER_SINGLE_TALK=21,
    USER_All_TALK=22

  };

}
extern QTcpSocket cli;
#endif // GLOBAL_H
